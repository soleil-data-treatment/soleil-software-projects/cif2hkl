# Makefile to compile cif2hkl.
# required: gfortran
#
# just type: make; sudo make install

# simple one-shot compile
all:	
	${FC} -ffree-line-length-512 \
	CFML/CFML_GlobalDeps_Linux.f90 CFML/CFML_Math_General.f90 \
	CFML/CFML_String_Utilities_gf.f90   CFML/CFML_Math_3D.f90 \
	CFML/CFML_Symmetry_Tables.f90        CFML/CFML_Scattering_Chemical_Tables.f90 \
	CFML/CFML_Crystallographic_Symmetry.f90         CFML/CFML_Crystal_Metrics.f90 \
	CFML/CFML_Reflections_Utilities.f90      CFML/CFML_Atom_TypeDef.f90 \
	CFML/CFML_Geometry_Calc.f90        CFML/CFML_Molecular_Crystals.f90 \
	CFML/CFML_Diffraction_Patterns.f90         CFML/CFML_Magnetic_Groups.f90 \
	CFML/CFML_EisPack.f90          CFML/CFML_IO_Formats.f90 \
	CFML/CFML_Structure_Factors.f90 -o cif2hkl cif2hkl.F90 -lm $(FFLAGS) $(LDFLAGS) $(CPPFLAGS)
	rm *.mod

help:
	@help2man --output=man/cif2hkl.1 --version-string=1.4.6 --no-discard-stderr --no-info --name="compute diffraction structure factors" ./cif2hkl || echo "WARNING: failed to generate man page"
	@pandoc README.md --to pdf > man/README.pdf || echo "WARNING: skip pdf doc (missing pandoc)"
	@echo "Usage: install cif2hkl"
	@echo "make all"
	@echo "  compile"
	@echo "make install"
	@echo "  install cif2hkl"
	@echo "make deb"
	@echo "  create debian package"

clean: 
	rm -f *.o *.mod cif2hkl

install:
	install -D cif2hkl $(DESTDIR)$(prefix)/usr/bin/cif2hkl
	install -D man/cif2hkl.1 $(DESTDIR)$(prefix)/usr/share/man/man1/cif2hkl.1
	install -D -d $(DESTDIR)$(prefix)/usr/share/doc/cif2hkl
	install -D man/README.pdf $(DESTDIR)$(prefix)/usr/share/doc/cif2hkl

distclean: clean

uninstall:
	-rm -f $(DESTDIR)$(prefix)/usr/bin/cif2hkl
	-rm -f $(DESTDIR)$(prefix)/usr/share/man/man1/cif2hkl.*
	-rm -fr $(DESTDIR)$(prefix)/usr/share/doc/cif2hkl

test:
	./cif2hkl --version
	./cif2hkl --verbose --xtal --no-output-files examples/CaF2.cfl
	@echo Test OK
	
deb:
	rm -rf debian
	cp -r packaging/debian .
	debuild -b || true
	rm -rf debian

.PHONY: all install clean distclean uninstall test deb

